;
; This file is part of WPS-PC.
; Copyright (C) 1996 - 2018 Canux Corporation
;
; WPS-PC is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; WPS-PC is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with WPS-PC.  If not, see <http://www.gnu.org/licenses/>.
;
;*********************************************************************
;
; 	WPS-PC USER KEY DEFINITION MODULE
;
; 	Copyright (c) 1985 Exceptional Business Solutions, Inc.
;
;*********************************************************************

	NAME UDK		 ;MODULE NAME

;MODULE HISTORY

;10/28/86 - 080 Fixed check on udk range and added check for ALT-U.

	INCLUDE EXTRN.ASM
	INCLUDE DOSMAC.ASM
	INCLUDE	WPSMAC.ASM

	PUBLIC	UDK$$
	PUBLIC	U_BUFFER

	EXTRN	P_HAN:WORD
	EXTRN	KBD_CHAR:NEAR
	EXTRN	GETIDX$$:NEAR
	EXTRN	H_SCREEN:WORD

DATA     SEGMENT	PUBLIC 'DATA'


FUNTBL	DW	W_ADV,W_BAK,W_RIGHT,W_LEFT,W_UP,W_DN,W_WORD,W_LINE
	DW	W_PARA,W_SENT,W_TABP,W_PAGE,W_ANGLE,W_GADV,W_GBAK,W_PGUP
	DW	W_PGDN,W_ADVW,W_BAKW,W_RUBC,W_RUBW,W_DELC,W_DELW,W_RUBL
	DW	W_RUBS,W_OOPS,W_TOP,W_BOT,W_FILE,W_GETDOC,W_BOLD,W_UDL
	DW	W_UCASE,W_GBOLD,W_GUDL,W_GUCASE,W_SUPR,W_SUB,W_OVS,W_CUT,W_PASTE
	DW	W_GCUT,W_GPASTE,W_SEL,W_CNTR,W_SWAP,W_HPUSH,W_HPULL,W_SRCH
	DW	W_CONTS,W_CONTSS,W_REPL,W_NEWP,W_PGMARK,W_NBSPC,W_PARMRK
	DW	W_BRKH,W_INVH,W_FIG,W_PCMD,W_HELP,W_HALT,W_VIEW,W_UDK
	DW	W_RULER,W_QUIT,W_STATUS,W_DATE,W_GPAGE,W_MENU,W_LIB,W_RCODE
	DW	W_INDEX

	MSG$	W_ADV,"ADVAN"
	MSG$	W_BAK,"BACKUP"
	MSG$	W_RIGHT,"RIGHT-ARROW"
	MSG$	W_LEFT,"LEFT-ARROW"
	MSG$	W_UP,"UP-ARROW"
	MSG$	W_DN,"DN-ARROW"
	MSG$	W_WORD,"WORD"
	MSG$	W_LINE,"LINE"
	MSG$	W_PARA,"PARA"
	MSG$	W_SENT,"SENT"
	MSG$	W_TABP,"TAB-POS"
	MSG$	W_PAGE,"PAGE"
W_ANGLE	DB	2,"<>"
	MSG$	W_GADV,"G-ADVAN"
	MSG$	W_GBAK,"G-BAK"
	MSG$	W_PGUP,"PgUp"
	MSG$	W_PGDN,"PgDn"
	MSG$	W_ADVW,"ADV-W"
	MSG$	W_BAKW,"BAK-W"
	MSG$	W_RUBC,"RUB-C"
	MSG$	W_RUBW,"RUB-W"
	MSG$	W_DELC,"DEL-C"
	MSG$	W_DELW,"DEL-W"
	MSG$	W_RUBL,"RUB-L"
	MSG$	W_RUBS,"RUB-S"
	MSG$	W_OOPS,"G-OOPS"
	MSG$	W_TOP,"G-TOP"
	MSG$	W_BOT,"G-BOT"
	MSG$	W_FILE,"G-FILE-DOCMT"
	MSG$	W_GETDOC,"G-GET-DOC"
	MSG$	W_BOLD,"BOLD"
	MSG$	W_UDL,"UNDER-LINE"
	MSG$	W_UCASE,"UPPER-CASE"
	MSG$	W_GBOLD,"G-BOLD"
	MSG$	W_GUDL,"G-UNDER-LINE"
	MSG$	W_GUCASE,"G-UPPER-CASE"
	MSG$	W_SUPR,"G_SUPER-SCRPT"
	MSG$	W_SUB,"G-SUB-SCRPT"
	MSG$	W_OVS,"G-DEAD"
	MSG$	W_CUT,"CUT"
	MSG$	W_PASTE,"PASTE"
	MSG$	W_GCUT,"G-CUT"
	MSG$	W_GPASTE,"G-PASTE"
	MSG$	W_SEL,"SEL"
	MSG$	W_CNTR,"G-CENTR"
	MSG$	W_SWAP,"G-SWAP"
	MSG$	W_HPUSH,"PUSH"
	MSG$	W_HPULL,"G-PULL"
	MSG$	W_SRCH,"G-SRCH"
	MSG$	W_CONTS,"G-CONT-SRCH"
	MSG$	W_CONTSS,"G-CONT-SRCH-&-SEL"
	MSG$	W_REPL,"G-REPLC"
	MSG$	W_NEWP,"G-NEW-PAGE"
	MSG$	W_PGMARK,"G-PAGE-MARK"
	MSG$	W_NBSPC,"G-SPACE"
	MSG$	W_PARMRK,"G-PARA"
	MSG$	W_BRKH,"G-BREAKING-HYPHEN"
	MSG$	W_INVH,"G-INVISIBLE-HYPHEN"
	MSG$	W_FIG,"G-FIGURE"
	MSG$	W_PCMD,"G-PRINT-COMND"
	MSG$	W_HELP,"G-HELP"
	MSG$	W_HALT," "
	MSG$	W_VIEW,"G-VIEW"
	MSG$	W_UDK,"G-UDK"
	MSG$	W_RULER,"G-RULER"
	MSG$	W_QUIT,"G-QUIT"
	MSG$	W_STATUS,"G-STATUS"
	MSG$	W_DATE,"G-DATE"
	MSG$	W_GPAGE,"G-PAGE"
	MSG$	W_MENU,"G-MENU"
	MSG$	W_LIB,"G-LIBRY"
	MSG$	W_RCODE," "
	MSG$	W_INDEX,"G-INDEX"

KEYNO	DW	0		;UDK KEY NUMBER, 0-99
KEYNO$	DB	0,0,0,0,0	;UDK NUMBER STRING
UCHAR	DW	0		;SAVED UDK CHAR
P$	DB	0, 20 DUP (?)
Q$	DB	0, 20 DUP (?)
R$	DB	0, 20 DUP (?)
S$	DB	0, 40 DUP (?)
ROW	DB	0		;POS ROW
COL	DB	0		;COL
NM_FLG	DB	0		;1=GOLD NUMBER
GLD_NUM DB	0		;GOLD NUMBER
UDKPTR	DW	0		;POINTER TO NEXT ENTRY IN BUFFER
UDKCNT	DW	0		;COUNT OF ENTRIES IN UDK BUFFER, MAX 127
UDKREC	DW	0		;RECORD NUMBER OF CURRENT UDK BUFFER
UDKLST	DW	0		;RECORD NUMBER OF LAST ALLOCATED UDK RECORD
MODE	DB	0		;0=MODIFY/DISP, 1=ENTER.
SAV_P_HAN DW	0		;SAVED PARAM FILE OPEN FLAG
U_SAVRC	DW	0		;SAVED ROW, COL
U_BUFFER DW	P_BUFLEN/2 DUP (?) ;INTERNAL UDK BUFFER
	MSG$	HMSG$,"HELP is not available during user key definition."

DATA  	ENDS

CODE	SEGMENT PUBLIC 'CODE'
	ASSUME	CS:CODE,DS:DATA,ES:DATA

UDK$$:
	SAVE	BX,CX,DX,SI,DI
	SAVE$	H_SCREEN		;SAVE THE CURRENT SCREEN
	MOV	U_SAVRC,DX		;SAVE CURSOR POS
	MVBX	SAV_P_HAN,P_HAN		;LEAVE PARAM FILE OPEN AT END?
MENU:	CLRSCN
	TITLE$ "Define User (Macro) Keys"
	DISPLAY	6,15,"D = Display/modify existing User Key sequence"
	DISPLAY	10,15,"E = Enter new User Key sequence"
	DISPLAY	20,10,"Type the letter, a space, and a number from 0 to 99,"
	DISPLAY	21,10,"or press GOLD MENU to exit from this menu."
MNU0:	CALL	KBDINP
	ERROR	UDK_EXIT,L
	IFZ	CMDBUF$,MNU0
	SCAN$	MNU$,CMDBUF$
	SCAN$	T$,CMDBUF$
	NUM$	T$
	ERROR	MENU
	VAL$	DX,T$
	MOV	KEYNO,DX
	STR$	KEYNO$,DX
;	IFGT	DX,99,RNG_ERR						;080
	IFGT	KEYNO,99,RNG_ERR					;080
	IFEQ$	MNU$,"D",DISP_UDK,L
	IFEQ$	MNU$,"E",ENTER_UDK,L
	LEFT$	MNU$,MNU$,60
	CLREOL	24,1
	DISPLAY	24,1,"Typing '"
	DISPLAY	0,0,MNU$
	DISPLAY	0,0,"' has no meaning here."
	BEEP
	GOTO	MNU0

RNG_ERR:
	DISPLAY	24,1,"Enter 'D' or 'E' and a number from 0 to 99."
	BEEP
	GOTO	MNU0

UDK_EXIT:
	IFNZ	SAV_P_HAN,UDK_EX1	;PARAM FILE WAS ALREADY OPEN
	CALL	CLSPRM			;ELSE CLOSE IT NOW
UDK_EX1:
	MOV	DX,U_SAVRC		;ROW, COL
	RESTORE$ H_SCREEN,DH,DL
	RESTORE	BX,CX,DX,SI,DI
	GOTO	KBD_WAIT

DISP_UDK:
	CONCAT$	S$,"Display/modify user key ",KEYNO$," sequence"
	MOV	AX,0
	JR	ENT1

ENTER_UDK:
	CONCAT$	S$,"Enter new sequence for user key ",KEYNO$
	MOV	AX,1			;"ENTER"
ENT1:	MOV	MODE,AL
	CALL	OPNPRM
	ERROR	UDK_EXIT		;CANT OPEN PARAM FILE
	MOV	AX,UDK$PRMRC		;UDK INDEX RECORD
	MOV	DX,OFFSET U_BUFFER
	READ$	AX,P_HAN,DX,P_BUFLEN	;READ UDK INDEX RECORD
	MOV	BX,U_BUFFER		;LAST ACTIVE RECORD PTR
	IFGE	BX,UDK$PRMRC,ENT0	;VALID REC NUMNER
	MOV	BX,UDK$PRMRC		;ELSE INIT TO PROPER PLACE IN PARAM FILE
ENT0:	MOV	UDKLST,BX
	MOV	BX,KEYNO		;KEY NUMBER
	SAL	BX,1			;WORD INDEX
	ADD	BX,OFFSET U_BUFFER+2	;INDEX INTO INDEX REC, SKIP REC COUNT
	MOV	AX,[BX]			;GET UDK RECORD
	MOV	UDKREC,AX		;SAVE RECORD FOR LATER WRITE
	IFNZ	AX,ENT2			;UDK ALREADY DEFINED
	INC	UDKLST			;NEXT UNASSIGNED BUFFER
	MOV	AX,UDKLST
	MOV	UDKREC,AX		;WRITE IT OUT AT THIS RECORD
	MOV	[BX],AX			;ENTER INTO PROPER PLACE IN INDEX REC
	MVBX	U_BUFFER,UDKLST		;SAVE UPDATED UDK LAST RECORD
	MOV	AX,UDK$PRMRC		;RECORD FOR THIS WRITE
	MOV	DX,OFFSET U_BUFFER
	WRITE$	AX,P_HAN,DX,P_BUFLEN	;WRITE OUT UPDATED INDEX RECORD
	JR	ENT3
ENT2:	IFNZ	MODE,ENT3		;ENTER NEW SEQ...ZAP THE RECORD
	READ$	AX,P_HAN,DX,P_BUFLEN	;READ PROPER RECORD
	IFNZ	AX,REDISP_UDK		;RECORD NOT EMPTY
ENT3:	MOV	BX,OFFSET U_BUFFER
	MOV	WORD PTR [BX],0		;ELSE MAKE THE RECORD EMPTY

REDISP_UDK:	
	CLRSCN
	DISPLAY	1,1,S$
	MOV	UDKPTR,OFFSET U_BUFFER
	MOV	ROW,2			;START REDISPLAY AT TOP OF SCREEN
	MOV	COL,1
	MOV	UDKCNT,0		;START WITH NO CHARS DISPLAYED

RED1:	MOV	BX,UDKPTR
	MOV	AX,[BX]			;GET NEXT ENTRY
     	IFZ	AX,RED2			;END OF SEQUENCE?
	ADD	BX,2
	MOV	UDKPTR,BX		;POINT TO NEXT ENTRY
	CALL	CONV_CHAR		;NOT END...CONVERT AND DISPLAY IT
	JR	RED1

RED2:	DISPLAY	21,1,"To add a key to the sequence, just press the key."
	DISPLAY	22,1,"Press GOLD-"
	DISPLAY	0,0,KEYNO$
	DISPLAY	0,0,"-UDK to delete the last key from the sequence."
	DISPLAY	23,1,HMSG$
	DISPLAY	24,1,"Press GOLD HALT to store this sequence and recall "
	TEXT	"the User Key Menu."
GETINP:	MOV	DX,127
	SUB	DX,UDKCNT
	STR$	R$,DX
	APPEND$	R$,"  "
	DISPLAY	1,50,"Characters remaining: "
	DISPLAY	0,0,R$		

	CURSOR	ROW,COL
GET2:	CALL	GOLD_HALT		;CHECK FOR HALT COMMAND
	ERROR	END_UDK,L
	CALL	KBD_CHAR		;GET NEXT KBD CHAR
	IFZ	AX,GET2			;LOOP UNTIL A CHAR
	IFLT	AL,128,GET0		;NOT FUNCTION KEY
	MOV	AH,AL
	SUB	AH,126
	MOV	AL,G_UDK		;MAKE IT INTO NUMBERED UDK REQ	
GET0:	MOV	UCHAR,AX		;SAVE INPUT CHAR AND FLAG
	IFZ	AH,GET1			;NOT GOLD FUNCTION
	IFEQ	AL,G_DEFUDK,BAD_UDK	;DONT ALLOW ALT-U		;080
	IFNE	AL,G_UDK,GET1		;NOT A UDK INVOCATION
	MOV	AL,AH
	IFEQ	AL,255
	   MOV	AL,1
	IFEND
	DEC	AL
	IFGT	AL,99,BAD_UDK		;RANGE 0-99
	CBW
	IFEQ	AX,KEYNO,DEL_UDK	;DELETE LAST UDK CHAR
	MOV	AX,UCHAR
GET1:	IFGE	UDKCNT,127,UDK_FULL	;NO MORE ROOM IN BUFFER
	CALL	CONV_CHAR
	ERROR	GETINP			;NO MORE ROOM ON SCREEN
	MOV	AX,UCHAR		;ELSE STORE IN BUFFER
	MOV	BX,UDKPTR
	MOV	[BX],AX			;INTO THE BUFFER
	ADD	BX,2
	MOV	UDKPTR,BX		;UPDATE PTR
	MOV	WORD PTR [BX],0		;STORE TERMINATOR
	GOTO	GETINP

UDK_FULL:
	BEEP
	GOTO	GETINP

END_UDK:
	MOV	AX,UDKREC		;RECORD NUMBER FOR THIS UDK BUFFER
	MOV	DX,OFFSET U_BUFFER
	WRITE$	AX,P_HAN,DX,P_BUFLEN	;WRITE OUT THE UPDATED UDK BUFFER
	GOTO	MENU			;BACK TO UDK MENU

BAD_UDK:
	BEEP
	GOTO	GETINP

DEL_UDK:	
	CMP	UDKPTR,OFFSET U_BUFFER
	JE	BAD_UDK			;BUFFER EMPTY
	MOV	BX,UDKPTR
	SUB	BX,2			;AND BACKUP THE BUFFER
	MOV	UDKPTR,BX
	MOV	WORD PTR [BX],0		;ZAP THE LAST ENTRY IN UDK BUFFER
	GOTO	REDISP_UDK		;AND REDISPLAY SHORTENED UDK SEQ


;ENTERED WITH UDK CHAR IN AX.  CONVERTS TO ASCII STRING REPRESENTATION OF CHAR
;AND DISPLAYS TEXT AT CURRENT ROW, COL.  THEN ADVANCES ROW, COL.  RETURNS CY=1
;ON UDK FULL.

CONV_CHAR:
	IFNZ	AH,WPSFUN		;FUNCTION KEY
	IFEQ	AL,13,CAR_RET		;CARRIAGE RETURN
	IFEQ	AL,9,TAB		;TAB
	IFLT	AL,32,CONV_EX,L		;CONTROL CHAR...IGNORE IT
	IFEQ	AL,32,SPC		;SPACE CHAR
	MOV	P$,1			;LENGTH OF 1
	MOV	P$+1,AL			;SAVE CHAR
	GOTO	COM			;AND PROCESS IT

CAR_RET:
	MOV$	P$,"RETURN"
	GOTO	COM

TAB:	MOV$	P$,"TAB"
	GOTO	COM

SPC:	MOV$	P$,"SPACE"
	GOTO	COM

WPSFUN:
	MOV 	GLD_NUM,0		;MAKE DEFAULT 0
	MOV	NM_FLG,0
	IFEQ	AH,255,WFUN1		;THERE IS NO NUMBER
	MOV	GLD_NUM,AH		;ELSE SAVE GOLD NUMBER
	DEC	GLD_NUM
	INC	NM_FLG
WFUN1:	ZAP	AH
	PUSH	AX			;SAVE G_FUN
	SAL	AL,1			;INDEX TABLE BY WORD
	MOV	BX,AX
	ADD	BX,OFFSET FUNTBL-2	;INDEX THE TABLE
	MOV	SI,[BX]
	MOV	DI,OFFSET P$
	CALL	MOV$$			;MOVE PROPER STRING INTO P$
	POP	AX			;GET G_FUN BACK
	IFZ	NM_FLG,COM		;NO NUMBER
	MOV$	Q$,P$			;SAVE ORIGINAL STRING
	IFEQ	AL,G_RULER,WFUN3	;RULER AND UDK ARE SPECIAL CASES
	IFNE	AL,G_UDK,WFUN4
WFUN3:	MID$	Q$,P$,3,10		;STRIP OFF "G-"
WFUN4:	MOV	AL,GLD_NUM		;GET NUMBER
	CBW
	STR$	R$,AX
	CONCAT$	P$,"G-",R$,"-",Q$	;MAKE IT INTO "G-n-CUT"
COM:	MOV	AL,P$			;GET LENGTH OF STRING
	ADD	AL,COL
	IFLE	AL,80,COM1
	IFGE	ROW,20,SCN_FULL		;TOO MUCH ON SCREEN...GOTTA STOP
	INC	ROW
	MOV	COL,1
	JR	COM
COM1:	PUSH	AX
	DISPLAY	ROW,COL,P$
	POP	AX
	INC	AL
	MOV	COL,AL
	INC	UDKCNT			;COUNT THE UDK SEQ JUST DISPLAYED
CONV_EX:
	CLC
	RET

SCN_FULL:
	BEEP
	STC
	RET


KBDINP:
	MOV	CMDBUF$,0	;ZERO CHAR COUNT
INP1:	MOV	AL,CMDBUF$
	INC	AL
	CURSOR	24,AL 		;POSIT TO COMMAND LINE
INP0:	CALL	KBD_CHAR	;INP NEXT CHAR
	IFZ	AX,INP0		;WAIT FOR SOME CHAR
	IFNZ	AH,FUN		;FUNCTION CODE
	MOV	CHAR,AL		;SAVE CHAR
	IFEQ	AL,13,INPEX,L	;DONE
	IFLT	AL,32,INP1	;CONTROL CHAR...IGNORE IT
	MOV	AL,CMDBUF$	;INP COUNT
	IFGT	AL,80,INP1	;BUFFER TOO FULL...IGNORE CHAR
	IFNZ	AL,INP3		;JUMP IF ALREADY GOT TEXT IN BUFFER
	CLREOL	24,1
INP3:	INC	CMDBUF$		;ONE MORE CHAR IN CMDBUF$
	MOV	BL,CMDBUF$
	ZAP	BH
	MOV	AL,CHAR
	MOV	CMDBUF$[BX],AL	;STORE CHAR INTO BUFFER
	CALL	DISP_CHAR	;OUTPUT CHAR
	GOTO 	INP1

FUN:	IFEQ	AL,G_MENU,FUNEX
	IFEQ	AL,G_RUBC,RUBC
	IFEQ	AL,G_RUBW,RUBW
	IFNE	AL,G_HELP,FUN1
	CLREOL	24,1
	DISPLAY	24,1,HMSG$
	BEEP
	GOTO	KBDINP
FUN1:	BEEP
	GOTO	INP1

FUNEX:	MOV	CMDBUF$,0
	STC
	RET

RUBC:	IFZ	CMDBUF$,INP1,L
	DEC	CMDBUF$		;ONE LESS CHAR
	CALL	BAKSPC$$	;BACK UP AND INP RID OF CHAR
	GOTO	INP1

RUBW:	IFZ	CMDBUF$,INP1,L
	DEC	CMDBUF$
	CALL	BAKSPC$$	;INP RID OF LAST CHAR
 	MOV	BL,CMDBUF$
	ZAP	BH
	MOV	AL,CMDBUF$[BX]	;INP LAST CHAR OF BUFFER
	IFNE	AL," ",RUBW	;LOOP IF NOT BLANK
	GOTO	INP1

INPEX:	MOV	DI,OFFSET CMDBUF$
	PUSH	DI		;SAVE BUFFER PTR
	CALL	CLIP$$		;INP RID OF LEADING BLANKS
	POP	DI
	CALL	STRIP$$		;INP RID OF TRAILING BLANKS
	CLC
	RET			;CMDBUF$ NOW SET UP


CODE	ENDS

	END
